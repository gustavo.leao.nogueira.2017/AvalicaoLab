/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blog.es.esy.gustavoti;

/**
 *
 * @author Gustavo Leão Nogueira de Oliveira
 * @version 1.33
 */
interface TenisFuncoes {
   
    public abstract int getNumero();
    public abstract void setNumero(int numero);
    public abstract String getCor();
    public abstract void setCor(String cor);
    public abstract double getValor();
    public abstract void setValor(double valor);

}
