/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blog.es.esy.gustavoti;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author Gustavo Leão Nogueira de Oliveira
 * @version 1.33
 * 
 */
public class Vendas {

    
    static Tenis vTenis[] = new Tenis[20];
    static String cores[]={"Branca", "Preta", "Prata", "Azul", "Vermelha", "Verde"};
    static String numeros[] = new String[24];
    static String precos[] = new String[49];
    static Random r = new Random();
    private static FileWriter fw;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        menu();
    }

    private static void menu() {
        String menu[] = new String[]{
            "1 - Cadastra Tenis",
            "2 - Lista tenis",
            "3 - Lista por números",
            "4 - Lista por cor",
            "5 - Sair"
        };
        switch(lerMenu(menu)){
            case 1: cadastra(); menu(); break;
            case 2: listaTodos(); menu(); break;
            case 3: listaNumeros(); menu(); break;
            case 4: listaCor();menu();  break;
            case 5: setArquivo(); escreva("Então até mais!"); break;
            default: escreva("Tente novamente! Digite um número valido!"); menu(); break;
        }
    }

    private static int lerMenu(Object[] menu) {
        int opcao = 0;
        try{
            opcao = JOptionPane.showOptionDialog(null, "Escolha:", "Menu", 0, JOptionPane.PLAIN_MESSAGE, null, menu, menu);
            switch (opcao) {
                case 0: opcao = 1; break;
                case 1: opcao = 2; break;
                case 2: opcao = 3; break;
                case 3: opcao = 4; break;
                case 4: opcao = 5; break;
                default: opcao = 0; break;
            }

        }catch(HeadlessException e){
            System.out.println(e.getMessage());
        }
        return opcao;
    }

    private static void listaCor() {
        String cor = lerCor("Escolha uma das cores:");
        String str = "Tenis pelo tamanho escolhido:"+"\n";
        int cont = 0;
        for (Tenis vTeni : vTenis) {
            if (vTeni.getCor().equals(cor)) {
                str += "Número: \t" + vTeni.getNumero() + "\t Valor: \t" + vTeni.getValor() + "\n";
            } else {
                cont ++;
            }
        }
        if(cont == 0 || cont == vTenis.length){
            str = "Nada foi encontrado!"+"\n";
        }
        escreva(str);  
    }

    private static void listaNumeros() {
        int numero = lerNumero("Escolha um número entre [22 - 45]:");
        String str = "Tenis pelo tamanho escolhido:"+"\n";
        int cont = 0;
        for (Tenis vTeni : vTenis) {
            if (vTeni.getNumero() == numero) {
                str += "Número: \t" + vTeni.getNumero() + "\t Cor: \t" + vTeni.getCor() + "\n";
            } else {
                cont++;
            }
        }
        if(cont == 0 || cont == vTenis.length){
            str = "Nada foi encontrado!"+"\n";
        }
        escreva(str);
    }

    private static void listaTodos() {
        String str = "Todos os tênis:"+"\n";
        double total = 0;
        for (Tenis vTeni : vTenis) {
            str += "Número: \t" + vTeni.getNumero() + "\t Cor: \t" + vTeni.getCor() + "\t Valor: \t" + vTeni.getValor() + "\n";
            total += vTeni.getValor();
        }
        str += "\t Total é igual a: "+total+"\n";
        escreva(str);
    }

    private static void cadastra() {
        for(int i = 0; i < vTenis.length; i++){
            vTenis[i] = new Tenis(lerNumero("Digite um número do tenis entre [22 - 45]:"),
                                  lerCor("Digite a cor do tenis:"),
                                  lerPreco("Digite o preço do tenis"));
        }
    }

    private static void escreva(String texto) {
        JOptionPane.showMessageDialog(null, texto);
    }

    private static int lerNumero(String texto) {
        int numero = 0;
        try{
            setNumeros();
            do{
                numero = Integer.parseInt((String) JOptionPane.showInputDialog(null, texto, "Tamanho", JOptionPane.QUESTION_MESSAGE, null, numeros, numeros[0]));
            }while(numero < 22 || numero > 45);
        }catch(HeadlessException | NumberFormatException e){
            escreva("Então escolha o que deseja fazer!");
            menu();
        }
        return numero;
    }
    private static String lerCor(String texto) {
        String cor = "";
        try{
            do{
                cor = (String) JOptionPane.showInputDialog(null, texto, "Cor", JOptionPane.QUESTION_MESSAGE, null, cores, cores[0]);
                if(cor == null ){ escreva("Então escolha o que deseja fazer:"); menu();}
            }while(cor == null || verificarCor(cor) == false);
        }catch(HeadlessException e){
            System.out.println(e.getMessage());
            
        }
        return cor;
    }
    private static double lerPreco(String texto) {
        double preco = 0;
        try{
            setPrecos();
            preco = Double.parseDouble((String) JOptionPane.showInputDialog(null, texto, "Preço", JOptionPane.QUESTION_MESSAGE, null, precos, precos[0]));   
        }catch(HeadlessException | NumberFormatException e){
            escreva("Então escolha o que deseja fazer!");
            menu();
        }
        return preco;        
    }

    private static void setNumeros() {
            int cont = 22;
            for(int i = 0; i < numeros.length; i++){
                numeros[i]=""+cont;
                if(cont < 45){cont++;}
            }    
    }

    private static boolean verificarCor(String cor) {
        boolean teste = false;
        for (String core : cores) {
            if (core.equals(cor)) {
                teste = true;
            }
        }
        return teste;
    }

    private static String[] setPrecos() {
        for (int i = 0; i < precos.length; i++) {
            precos[i] = ""+Math.floor(r.nextDouble() * 100);
        }
        return precos;
    }

    private static void setArquivo() {
        try {
            fw = new FileWriter(new File("banco.db"));
            for (Tenis vTeni : vTenis) {
                fw.append("Número: \t" + vTeni.getNumero() + "\t Cor: \t" + vTeni.getCor() + "\t Valor: \t" + vTeni.getValor() + "\n");
            }
            fw.flush();
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
        finally{
            try {
                fw.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            
        }
    }

    
}
