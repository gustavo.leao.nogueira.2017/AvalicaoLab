/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blog.es.esy.gustavoti;

/**
 *
 * @author Gustavo Leão Nogueira de Oliveira
 * @version 1.33
 */
public class Tenis implements TenisFuncoes{
        private int numero;
        private String cor;
        private double valor;

    Tenis() {
        
    }

        @Override
    public int getNumero() {
        return numero;
    }

        @Override
    public void setNumero(int numero) {
        this.numero = numero;
    }

        @Override
    public String getCor() {
        return cor;
    }

        @Override
    public void setCor(String cor) {
        this.cor = cor;
    }

    /**
     *
     * @return
     */
    @Override
    public double getValor() {
        return valor;
    }

        @Override
    public void setValor(double valor) {
        this.valor = valor;
    }

    public Tenis(int numero, String cor, double valor) {
        this.numero = numero;
        this.cor = cor;
        this.valor = valor;
    }
        
        
        
}
